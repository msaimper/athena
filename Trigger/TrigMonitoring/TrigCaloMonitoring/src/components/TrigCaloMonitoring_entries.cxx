
#include "TrigCaloMonitoring/HLTCaloTool.h"
#include "TrigCaloMonitoring/HLTCaloFEBTool.h"
#include "TrigCaloMonitoring/HLTCaloToolL2.h"
#include "TrigCaloMonitoring/HLTCaloClusterTool.h"
#include "TrigCaloMonitoring/HLTCaloESD_xAODTrigEMClusters.h"
#include "TrigCaloMonitoring/HLTCaloESD_xAODCaloClusters.h"
#include "TrigCaloMonitoring/HLTCaloESD_CaloCells.h"


DECLARE_COMPONENT( HLTCaloTool )
DECLARE_COMPONENT( HLTCaloFEBTool )
DECLARE_COMPONENT( HLTCaloToolL2 )
DECLARE_COMPONENT( HLTCaloClusterTool )
DECLARE_COMPONENT( HLTCaloESD_xAODTrigEMClusters )
DECLARE_COMPONENT( HLTCaloESD_xAODCaloClusters )
DECLARE_COMPONENT( HLTCaloESD_CaloCells )

