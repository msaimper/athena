#include "../SiPropertiesSvc.h"
#include "../SiPropertiesCHSvc.h"
#include "../SiPropertiesTool.h"
#include "../SCTSiPropertiesCondAlg.h"
#include "../PixelSiPropertiesCondAlg.h"

DECLARE_COMPONENT( SiPropertiesSvc )
DECLARE_COMPONENT( SiPropertiesCHSvc )
DECLARE_COMPONENT( SiPropertiesTool )
DECLARE_COMPONENT( SCTSiPropertiesCondAlg )
DECLARE_COMPONENT( PixelSiPropertiesCondAlg )

