#include "DerivationFrameworkMCTruth/TruthDressingTool.h"
#include "DerivationFrameworkMCTruth/TruthIsolationTool.h"
#include "DerivationFrameworkMCTruth/MenuTruthThinning.h"
#include "DerivationFrameworkMCTruth/GenericTruthThinning.h"
#include "DerivationFrameworkMCTruth/TruthCollectionMaker.h"
#include "DerivationFrameworkMCTruth/TruthCollectionMakerTau.h"
#include "DerivationFrameworkMCTruth/TruthClassificationDecorator.h"
#include "DerivationFrameworkMCTruth/CompactHardTruth.h"
#include "DerivationFrameworkMCTruth/HardTruthThinning.h"

#include "DerivationFrameworkMCTruth/HadronOriginDecorator.h"
#include "DerivationFrameworkMCTruth/HadronOriginClassifier.h"

using namespace DerivationFramework;

DECLARE_COMPONENT( TruthDressingTool )
DECLARE_COMPONENT( TruthIsolationTool )
DECLARE_COMPONENT( MenuTruthThinning )
DECLARE_COMPONENT( GenericTruthThinning )
DECLARE_COMPONENT( TruthCollectionMaker )
DECLARE_COMPONENT( TruthCollectionMakerTau )
DECLARE_COMPONENT( TruthClassificationDecorator )
DECLARE_COMPONENT( CompactHardTruth )
DECLARE_COMPONENT( HardTruthThinning )

DECLARE_COMPONENT( HadronOriginDecorator )
DECLARE_COMPONENT( HadronOriginClassifier )

