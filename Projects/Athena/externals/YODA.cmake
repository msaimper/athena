#
# File specifying the location of YODA to use.
#

set( YODA_VERSION 1.7.0 )
set( YODA_ROOT
   ${LCG_RELEASE_DIR}/MCGenerators/yoda/${YODA_VERSION}/${LCG_PLATFORM} )
